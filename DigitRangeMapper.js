'use strict';

function DigitRangeMapper(){

  // Easy method for the -> format
  function formatRange(start, end){
    if (typeof start !== 'number' || typeof end !== 'number'){
      console.log("Non-number passed to formatRange, don't do this");
      return ""
    }
    return String(start) + "->" + String(end);
  }

// Figures out which sort of gap we have (none, single-digit, or range)
  function fillGaps(start, end){
    if (typeof start !== 'number' || typeof end !== 'number'){
      console.log("Non-number passed to fillGaps, don't do this");
      return ""
    }
    var diff = end -start;
    if (diff<=1){
      return null;
    }
    if (diff === 2){
      return String((start+end)/2);
    }
    if (diff>2){
      return formatRange(start+1, end-1);
    }
  }

/*
It's easier to do this programmatically if we just pretend the first out-of-range
value on each end is also in the array.
*/
  function padInputArray(inputArray, lowerBound, upperBound){
    var paddedArray = inputArray.slice();
    paddedArray.unshift(lowerBound-1);
    paddedArray.push(upperBound+1);
    return paddedArray;
  }

  function findGapsInDigitRange(inputArray, lowerBound, upperBound){
    // Default to original parameters.
    if (lowerBound == undefined){
      lowerBound = 0;
    }
    if (upperBound == undefined){
      upperBound = 99;
    }
    var startIndex = 0;
    var endIndex = 1;
    var paddedArray = padInputArray(inputArray, lowerBound, upperBound);
    var resultArray = [];
    while(endIndex<paddedArray.length){
      var element1 = paddedArray[startIndex];
      var element2 = paddedArray[endIndex];
      var gapString = fillGaps(element1, element2);
      if (gapString!=null){
        resultArray.push(gapString);
      }
      startIndex++;
      endIndex++;
    }
    return resultArray;
  }


  return{
    findGapsInDigitRange: findGapsInDigitRange,
  };
}

// ----------------------------Testing---------------------------------------
var Distilled = window.Distilled;
var assert = window.assert;


var suite = new Distilled();

function arrayCompare(array1, array2){
  if (!Array.isArray(array1) || !Array.isArray(array2)){
    return false;
  }
  if (array1.length !== array2.length){
    return false;
  }
  for (var i=0; i<array1.length; i++){
    if (array1[i]!== array2[i]){
      return false;
    }
  }
  return true;
}

function testArray(inputArray, expectedResult, lowerBound, upperBound){
  var mapper = new DigitRangeMapper();
  var actualResult = mapper.findGapsInDigitRange(inputArray, lowerBound, upperBound);
  if (!arrayCompare(expectedResult, actualResult)){
    throw new Error("Result array was not as expected");
  }
}

suite.test('Standard case', function(){
  var inputArray = [0, 1, 3, 50, 75];
  var expectedResult = ["2", "4->49", "51->74", "76->99"];
  testArray(inputArray, expectedResult);
});

suite.test('Empty case', function(){
  var inputArray = [];
  var expectedResult = ["0->99"];
  testArray(inputArray, expectedResult);
});

suite.test('Single digit', function(){
  var inputArray = [50];
  var expectedResult = ["0->49", "51->99"];
  testArray(inputArray, expectedResult);
});

suite.test('Consecutive integers', function(){
  var inputArray = [0,1,2,3,4,5,6,7,8,9];
  var expectedResult = ["10->99"];
  testArray(inputArray, expectedResult);
});

suite.test('New range', function(){
  var inputArray = [0, 5, 9];
  var expectedResult = ["1->4", "6->8", "10"];
  testArray(inputArray, expectedResult, 0, 10);
});
